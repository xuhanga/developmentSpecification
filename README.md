####   前端组 项目开发规范

######  This repository contains the specifications.

##### 项目流程规范

- [项目开发流程规范](项目开发流程规范.md) <span class="std-rec">[1.0]</span>


##### 前端编码规范

- [HTML编码规范](html-style-guide.md) <span class="std-rec">[1.0]</span>

- [CSS编码规范](css-style-guide.md) <span class="std-rec">[1.0]</span>